/* eslint-disable no-undef */
describe('authenticating to the app', () => {
  it('should be able to register a user', () => {
    cy.visit('/');
    cy.contains('register');
    cy.get('input[name=username]').type('cypress');
    cy.get('input[name=password]').type('c');
    cy.get('input[name=confirmPassword]').type('c');
    cy.contains('Register').click();
  });
  it.only('should be able to log a user in', () => {
    cy.visit('/');
    cy.contains('login').click();
    cy.get('input[name=username]').type('cypress');
    cy.get('input[name=password]').type('c');
    cy.contains('log in').click();
    cy.contains('Welcome');
  });
});

// add new tests here
// it.only("should use the custom login command to drive me to work", () => {
//     cy.login("cypress", "c");
//     cy.contains("Welcome to Monotpoly");
//     cy.pause();
//     //now we develop our app, perhaps using TDD
// });
